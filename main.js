document.getElementById("resultatSelectionne").innerHTML = 'Sélectionner un annonceur';

function myToggle() {
    const modal = document.getElementById("modalSearch");
    if (modal.style.display === "block") {
        modal.style.display = "none";
    } else {
        modal.style.display = "block";
    }
}


function searchResultat() {
    document.getElementById('array-resultat').innerHTML = ""
    let saisie = document.getElementById("inputSearch").value;
    axios
        .get('https://api.rocketfid.com/cds/kjIU8897j87jhjbn/' + saisie + '/', {
            headers: {
                'X-Instance': 'cdiscountpentest',
                'content-type': 'json_pp'
            }
        })
        .then(response => {
            let resultats = response.data
            for (let i = 0; i < resultats.length; i++) {
                document.getElementById('array-resultat').innerHTML += "<a onclick=\"objectSelect()\" class='resultat'>" + resultats[i] + "</a>"
            }
        })
}

function objectSelect() {
    document.getElementById("resultatSelectionne").innerHTML = event.originalTarget.text;
    document.getElementById("modalSearch").style.display = "none";
}
